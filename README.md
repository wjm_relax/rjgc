
项目简介
基于ROS + gazebo的智能机器人设计开发与模拟项目主要目的是实现小车的定点导航,采用的方案是通过将RGB-D相机采集到的深度图像转换为激光雷达数据，然后采用gmapping算法实现建图。安装navigation功能包实现导航小车的自主导航.urdf文件为sw插件导出的文件,其中添加了激光雷达,RGB-D相机和滑动转向驱动插件.激光雷达可以实现扫描建图,RGB-D相机实现深度建图,滑动转向驱动为小车提供移动功能.world文件为构建的地图文件.   


可运行文件为文件夹<<可移动小车>>里面的caa文件夹里面的内容   



                   							 	 ROS自动导航小车系统
简介：本程序为小车自动避障导航系统，使用RGB-D相机采集到的深度图像转换为激光雷达数据，然后采用gmapping算法实现建图，通过滑动移动插件实现移动。   
文件为整个模型车的所有文件,系统采用的是ubuntu18.04,需要安装三个功能包,gmapping功能包,depthimage_to_laserscan功能包和navigation功能包.   

一.安装ubuntu系统   
    1.虚拟机安装ubuntu方法   
   	参考链接：[对应链接]( http://985.so/a7t6)  
	下载时只需要把版本改成18.04,教程里面是14.04.   
    (1).下载Ubuntu镜像文件   
        下载地址：http://www.ubuntu.com      
    (2).VMware下载安装   
        1.通过百度搜索VMware安装   
        2.安装完成之后启动VMware，需要输入产品密钥：5A02H-AU243-TZJ49-GTC7K-3C61N（此密钥为网上搜索得到）   
    (3).在VMware安装Ubuntu   
        1.创建虚拟机   
        2.向导选择自定义   
        3.然后下一步，直到安装操作系统，稍后再安装系统   
        4.然后选择linux，注意这里下面的下拉选择Ubuntu64，因为我们下载的是64位的，如果你的电脑是32位的，就选Ubuntu即可   
        5.选择安装位置，这里必须输入一个已存在的目录，不然后面会报错的   
        6.后面设置处理器和内存的，电脑配置好的可以试试，否则采用默认的，博主这里是采用默认的，然后下一步，直到指定磁盘容量，选择将虚拟机存储为单个磁盘   
        7.然后下一步，到已准备好创建虚拟机，点击自定义硬件   
        8.然后在新CD/DVD(SATA)选项中，选择使用ISO映像文件，选择我们第一步下载的Ubuntu镜像   
        9.然后点击完成，向导设置完成   
        10.虚拟机已经配置完毕，接下来我们开启虚拟机   
        11.然后就会来到欢迎界面我们选择中文简体点击Install Ubuntu   
        12.选择是否安装第三方软件，都不选直接点击继续   
        13.点击清除整个磁盘并安装ubuntu，点击现在安装   
        14.在弹出的窗口中 选择继续   
        15.选择时区——上海，点击地图中的中国区域，然后继续   
        16.键盘布局选择汉语，继续   
        17.设置用户名密码   
        18.设置完毕后就开始正式安装   
    2.用软碟通制作Ubuntu启动盘方法(双系统ubuntu）   
        	参考链接:[对应链接](http://985.so/a7t0)    
	 (1).用UltraISO制作Ubuntu18.04 U盘启动盘   
            注（Ubuntu16.04可同理制作哦）   
            （1）清空U盘内文件，格式化U盘。   
            （2）从Ubuntu官网（https://ubuntu.com/）下载系统ios文件到电脑   
            （3）选择18.04LTS，开始下载(注意下载后缀为.ios)   
            （4）打开软碟通（UltraISO），点击打开文件夹   
            （5）找到刚才下载的ubuntu18.04 ISO 所在的文件夹，选中并点击打开   
            （6）点击“启动”–>”写入硬盘映像”，写入方式选择USB-HDD+或者USB-HDD都可以   
            （7）点击“便携启动”->写入新的驱动器引导扇区->Syslinux   
            （8）根据弹窗选择“是”，等待一段时间后，即完成Ubuntu18.04启动盘的制作   
        (2).Ubuntu系统安装   
            1、 设置bios   
                （1） 插入系统启动盘，按下上位机电源键后，一直点击F2，进入bios。   
                （2） 选择boot，设置启动项的顺序，将USB HDD设置为第一位。   
                （3） 设置完成后，按下F10保存并退出。   
            2、安装ubuntu   
                （1）按下f10保存重启后，选择Ubuntu install   
                （2）选择系统语言，选择English，然后点击continue   
                （3）选择是否连接网络，选择第一项不连接，然后点击continue。   
                （4）选择是否安装第三方软件，都不选直接点击continue。   
                （5）选择第一个选项Erase Ubuntu 16.04.2 LTS and reinsrall，然后continue   
                （6）弹出确认界面，点击continue。   
                （7）选择时区——上海，点击地图中的中国区域，然后继续continue。   
                （8）选择键盘布局，都选择English就可以了，然后选择continue。   
                （9）设置用户名、机器名、密码，设置完毕后开始正式安装   
二.安装ros系统,创建工作目录,教程如下   
    参考链接：[对应链接](http://985.so/a73t)   
    参考视频：[对应链接](http://985.so/a7pe)   
    1.找到‘软件和更新’，修改里面的下载源。点击桌面的ubuntu软件图表，然后在左上角ubuntu软件上点击左键，选择软件和更新
    2.选择中国的服务器，把第二项《其他软件》里的勾勾全部去掉，否则有可能在关闭时弹出“下载软件仓库信息失败 检查您的网络连接"   
    3.设置sources.list（设置软件源），将电脑设置为可以从http://packages.ros.org接收软件，在终端中输入sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'   
    4、添加秘钥，sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654    
    5、确保软件更新为最新版本，终端输入sudo apt-get update    
    6、下载ros，终端输入sudo apt-get install ros-melodic-desktop-full    
    7、初始化操作，需要先初始化rosdep。rosdep能够轻松地安装要编译的源代码的系统依赖关系，并且需要在ROS中运行一些核心组件。在使用 ROS 之前，需要先初始化 rosdep。rosdep 使得你可以为你想要编译的源码，以及需要运行的 ROS 核心组件，简单地安装系统依赖。终端输入sudo rosdep init和rosdep update    
    8、环境配置，添加ros环境变量。 如果在每次一个新的终端启动时，ROS 环境变量都能自动地添加进你的 bash 会话是非常方便，这可以通过如下三个命令来实现：echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc，echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc，source ~/.bashrc    
    9、终端输入sudo apt install ros-melodic-desktop-full安装ros系统    
三.创建工作空间     
    1.创建目录    
        我们需要创建一个工作空间目录，并进入到这个工作空间下。我的工作空间名字叫 catkin_ws（跟着ros官网的例子创建的）。打开终端执行以下代码。：   
        mkdir ‐p ~/catkin_ws/src    
        cd catkin_ws   
    2.编译    
        在进入工作空间后，我们需要先对工作空间进行编译。指令如下：catkin_make    
四.在创建的工作空间src下安装gmapping功能包,depthimage_to_laserscan功能包和navigation功能包    
    安装gmapping功能包命令为     
        git clone https://github.com/ros/geometry2/tree/melodic-devel    
        git clone https://github.com/ros-planning/navigation/tree/melodic-devel    
        git clone https://github.com/ros-planning/navigation_msgs.git    
        git clone https://github.com/ros-perception/slam_gmapping.git    
        git clone https://github.com/ros-perception/openslam_gmapping    
    安装depthimage_to_laserscan功能包        
        命令为git clone --branch indigo-devel  https://github.com/ros-perception/depthimage_to_laserscan    
    安装navigation功能包    
        命令为git clone https://github.com/ros-planning/navigation.git    
    下不下来的可以手动下载到目录/catkin_ws/src/,压缩文件为ZIP文件,并解压在创建的工作空间里面的src文件夹下    
五.在任意位置打开终端运行roslaunch cca gmapping_nav.launch    
六.点击rviz上方的2D Nav Goal选择地点即可进行导航    

